"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.routes = void 0;
const routes_types_1 = require("./routes.types");
const auth_routes_1 = __importDefault(require("../auth/auth.routes"));
const admin_handler_1 = __importDefault(require("../admin/admin-handler"));
exports.routes = [
    new routes_types_1.Route("/auth", auth_routes_1.default),
    new routes_types_1.Route("/admin", admin_handler_1.default)
];
