"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const trainer_schema_1 = require("./trainer.schema");
const create = (trainer) => trainer_schema_1.TrainerSchema.create(trainer);
const findOne = (cb) => trainer_schema_1.TrainerSchema.findOne(cb);
const findAll = () => trainer_schema_1.TrainerSchema.findAll();
exports.default = {
    create,
    findOne,
    findAll,
};
