"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrainerSchema = void 0;
const uuid_1 = require("uuid");
class TrainerSchema {
    static create(trainer) {
        try {
            const id = (0, uuid_1.v4)();
            const trainerRecord = Object.assign(Object.assign({}, trainer), { id });
            TrainerSchema.trainers.push(trainerRecord);
            return trainerRecord;
        }
        catch (e) {
            throw { message: "record not created", stack: e };
        }
    }
    static findOne(cb) {
        return TrainerSchema.trainers.find(cb);
    }
    static findAll() {
        return TrainerSchema.trainers;
    }
}
exports.TrainerSchema = TrainerSchema;
TrainerSchema.trainers = [];
