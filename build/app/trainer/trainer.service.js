"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const trainer_repo_1 = __importDefault(require("./trainer.repo"));
const create = (trainer) => trainer_repo_1.default.create(trainer);
const findOne = (cb) => trainer_repo_1.default.findOne(cb);
exports.default = {
    create,
    findOne
};
