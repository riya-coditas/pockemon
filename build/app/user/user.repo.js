"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_schema_1 = require("./user.schema");
const create = (user) => user_schema_1.UserSchema.create(user);
const findOne = (cb) => user_schema_1.UserSchema.findOne(cb);
exports.default = {
    create,
    findOne
};
