"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = void 0;
const uuid_1 = require("uuid");
class UserSchema {
    static create(user) {
        try {
            const id = (0, uuid_1.v4)();
            const userRecord = Object.assign(Object.assign({}, user), { id });
            UserSchema.users.push(userRecord);
            return userRecord;
        }
        catch (e) {
            throw { message: "record not created", stack: e };
        }
    }
    static findOne(cb) {
        return UserSchema.users.find(cb);
    }
}
exports.UserSchema = UserSchema;
UserSchema.users = [];
