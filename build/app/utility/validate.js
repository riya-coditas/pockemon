"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validate = exports.query = exports.params = exports.body = void 0;
const express_validator_1 = require("express-validator");
const check = (part, field) => (req, res, next) => {
    if (req[part].hasOwnProperty(field)) {
        return next();
    }
    next({ message: `${field} is required` });
};
const body = (field) => check("body", field);
exports.body = body;
const params = (field) => check("params", field);
exports.params = params;
const query = (field) => check("query", field);
exports.query = query;
const validate = (req, res, next) => {
    const validResult = (0, express_validator_1.validationResult)(req);
    if (!validResult.isEmpty()) {
        //status code for bad request
        // return res.status(400).send(new ResponseHandler(null,validResult))
        return next({ statusCode: 400, errors: validResult.array() });
    }
    next();
};
exports.validate = validate;
