"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const trainer_service_1 = __importDefault(require("../trainer/trainer.service"));
const auth_responses_1 = require("./auth.responses");
const bcryptjs_1 = require("bcryptjs");
const trainer_service_2 = __importDefault(require("../trainer/trainer.service"));
const encryptUserPassword = (trainer) => __awaiter(void 0, void 0, void 0, function* () {
    const salt = yield (0, bcryptjs_1.genSalt)(10);
    const hashedPassword = yield (0, bcryptjs_1.hash)(trainer.password, salt);
    trainer.password = hashedPassword;
    return trainer;
});
const register = (trainer) => __awaiter(void 0, void 0, void 0, function* () {
    trainer = yield encryptUserPassword(trainer);
    return trainer_service_1.default.create(trainer);
});
const login = (credential) => __awaiter(void 0, void 0, void 0, function* () {
    const trainer = trainer_service_2.default.findOne(t => t.email === credential.email);
    if (!trainer)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const isPasswordValid = yield (0, bcryptjs_1.compare)(credential.password, trainer.password);
    if (!isPasswordValid)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const { password } = trainer, userClone = __rest(trainer, ["password"]);
    return userClone;
});
exports.default = {
    register,
    login
};
