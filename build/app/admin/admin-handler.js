"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const trainer_schema_1 = require("../trainer/trainer.schema");
const admin_responses_1 = require("./admin.responses");
const admin_schema_1 = require("./admin.schema");
const router = (0, express_1.Router)();
router.get("/", (req, res, next) => {
    const logincredentials = req.body;
    const result = admin_schema_1.AdminSchema.login(logincredentials);
    res.send(result);
});
router.get("/findAll", (req, res, next) => {
    const logincredentials = req.body;
    const result = admin_schema_1.AdminSchema.login(logincredentials);
    if (result)
        res.send(trainer_schema_1.TrainerSchema.findAll());
    else {
        res.send(admin_responses_1.ADMIN_RESPONSES.INVALID_CREDENTIALS);
    }
});
router.get('/findOne', (req, res, next) => {
    const traineremail = req.body;
    const result = admin_schema_1.AdminSchema.findRecord(traineremail);
    if (result)
        res.send(result);
    else {
        res.send(admin_responses_1.ADMIN_RESPONSES.INVALID_CREDENTIALS);
    }
});
exports.default = router;
