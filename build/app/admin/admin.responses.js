"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ADMIN_RESPONSES = void 0;
exports.ADMIN_RESPONSES = {
    INVALID_CREDENTIALS: {
        statusCode: 400,
        message: "Invalid Credentials"
    }
};
