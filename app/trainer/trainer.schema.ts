import { ITrainer, TrainerPredicate, Trainers } from "./trainer.types";
import { v4 } from "uuid";

export class TrainerSchema {
    private static trainers: Trainers = [];

    static create(trainer: ITrainer) {
        try {
            const id = v4();
            const trainerRecord = { ...trainer, id }
            TrainerSchema.trainers.push(trainerRecord);
            return trainerRecord;
        } catch (e) {
            throw { message: "record not created", stack: e }
        }
    }

    static findOne(cb: TrainerPredicate) {
        return TrainerSchema.trainers.find(cb);
    }
    
    static findAll():Trainers{
            return TrainerSchema.trainers;
    }
}