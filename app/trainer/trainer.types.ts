export interface ITrainer {
    id: string;
    email: string;
    password: string;
    pockemon: string[];
}

export type Trainers = ITrainer[];

export type TrainerPredicate = (t: ITrainer) => boolean