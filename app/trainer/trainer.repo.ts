import { TrainerSchema } from "./trainer.schema";
import { ITrainer, TrainerPredicate } from "./trainer.types";


const create = (trainer: ITrainer) => TrainerSchema.create(trainer);

const findOne = (cb: TrainerPredicate) => TrainerSchema.findOne(cb);

const findAll = () => TrainerSchema.findAll();

export default {
    create,
    findOne,
    findAll,
}