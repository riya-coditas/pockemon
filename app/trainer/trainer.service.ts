import trainerRepo from "./trainer.repo";
import { ITrainer, TrainerPredicate } from "./trainer.types";

const create = (trainer: ITrainer) => trainerRepo.create(trainer);

const findOne = (cb: TrainerPredicate) => trainerRepo.findOne(cb);

export default {
    create,
    findOne
}