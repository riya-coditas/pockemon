import { NextFunction, Request, Response, Router } from 'express';
import { ITrainer } from '../trainer/trainer.types';
import { ResponseHandler } from '../utility/response-handler';
import { body } from '../utility/validate';
import authService from './auth.service';
import { LoginValidator } from './auth.validation';

const router = Router();

// register a user

router.post("/register", async (req, res, next) => {
    try {
        const trainer = req.body;
        const result: ITrainer = await authService.register(trainer);
        res.send(new ResponseHandler(result));
    } catch (e) {
        next(e);
    }
});

router.post("/login", LoginValidator, async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const credentials = req.body;
        const result: Omit<ITrainer, "password"> = await authService.login(credentials);
        res.send(new ResponseHandler(result));
    } catch (e) {
        next(e);
    }
});

export default router;