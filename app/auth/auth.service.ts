import userService from "../trainer/trainer.service";
import { ITrainer } from "../trainer/trainer.types";
import { AUTH_RESPONSES } from "./auth.responses";
import { ICredentials } from "./auth.types";
import { genSalt, hash, compare } from 'bcryptjs';
import trainerService from "../trainer/trainer.service";

const encryptUserPassword = async (trainer: ITrainer) => {
    const salt = await genSalt(10);
    const hashedPassword = await hash(trainer.password, salt);
    trainer.password = hashedPassword;

    return trainer;
}

const register = async (trainer: ITrainer) => {
    trainer = await encryptUserPassword(trainer);
    return userService.create(trainer);
}

const login = async (
    credential: ICredentials
) => {
    const trainer = trainerService.findOne(t => t.email === credential.email);

    if(!trainer) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

    const isPasswordValid = await compare(credential.password, trainer.password);
    if(!isPasswordValid) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

    const { password, ...userClone } = trainer;

    return userClone;
};

export default {
    register,
    login
}