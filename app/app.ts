import express from 'express';
import { registerRoutes } from './routes/routes';


export const startServer = () => {
    const app = express();

    registerRoutes(app);

    const { PORT } = process.env;
    app.listen(
        PORT || 5000,
        () => console.log(`SERVER ON PORT: ${PORT || 5000}`)
    )
}