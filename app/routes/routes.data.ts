import { Routes, Route } from "./routes.types";
import AuthRouter from '../auth/auth.routes';
import adminRouter from '../admin/admin-handler'

export const routes: Routes = [
    new Route("/auth", AuthRouter),
    new Route("/admin",adminRouter)
];