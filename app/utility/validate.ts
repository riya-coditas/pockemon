import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";

type Part = "body" | "query" | "params";

const check = ( part : Part, field : string) => 
(req:Request, res: Response, next : NextFunction) => {

    if(req[part].hasOwnProperty(field)){
        return next();
    }

    next({message : `${field} is required`});
}

export const body = (field:string) => check("body",field);
export const params = (field:string) => check("params",field);
export const query = (field:string) => check("query",field);

export const validate = (req:Request, res: Response, next : NextFunction) => {

    const validResult = validationResult(req)

    if(!validResult.isEmpty()){
        //status code for bad request
        // return res.status(400).send(new ResponseHandler(null,validResult))

        return next({statusCode : 400, errors : validResult.array()})
    }

    next();
}