import {Router} from "express";
import { Request } from "express-validator/src/base";
import {  TrainerSchema } from "../trainer/trainer.schema"
import { ITrainer } from "../trainer/trainer.types";
import { ADMIN_RESPONSES } from "./admin.responses";
import { AdminSchema } from "./admin.schema";

const router = Router();


router.get("/",(req,res,next)=>{
          const logincredentials= req.body
          const result = AdminSchema.login(logincredentials);
          res.send(result);
    })

router.get("/findAll",(req,res,next)=>{
  const logincredentials= req.body
  const result = AdminSchema.login(logincredentials)
  if(result)
      res.send(TrainerSchema.findAll())
  else{
    res.send(ADMIN_RESPONSES.INVALID_CREDENTIALS);
  }
})

router.get('/findOne',(req,res,next)=>{
  const traineremail = req.body;
  const result:Omit<ITrainer, "id"|"email"|"password"> = AdminSchema.findRecord(traineremail);
  if(result)
      res.send(result)
  else{
    res.send(ADMIN_RESPONSES.INVALID_CREDENTIALS);
  }
})

export default router;





