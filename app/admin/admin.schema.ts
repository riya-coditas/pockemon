import { TrainerSchema } from "../trainer/trainer.schema";
import trainerService from "../trainer/trainer.service";
import { Trainers } from "../trainer/trainer.types";
import { IAdmin} from "./admin.types";
import { ICredentials } from "../auth/auth.types";
import { ADMIN_RESPONSES } from "./admin.responses";

const admin : IAdmin = {
    email : "admin@admin.com",
    password:"admin"
}

export class AdminSchema {
   
    static login = (Admin : IAdmin)=>{
    if(admin.email === Admin.email && admin.password === Admin.password)
    {
        return true;
    }
    else
    {
        return false;
    }

  }


  static findRecord=(credential: ICredentials)=>{
    const trainer = trainerService.findOne(t => t.email === credential.email);
    if(!trainer) throw ADMIN_RESPONSES.INVALID_CREDENTIALS;
    
    const { id, email,password,...trainerClone } = trainer;

    return trainerClone;
  }
  
}
